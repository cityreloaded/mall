package de.cityreloaded.mall.commands;

import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import de.cityreloaded.mall.Mall;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import java.util.UUID;

/**
 * Created by Lars on 06.11.2016.
 */
public class AddMemberCommand extends AbstractCommand {

  @Override
  public String getCommand() {
    return "addmember";
  }

  @Override
  public String[] getAliases() {
    return new String[]{"am"};
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"mall"};
  }

  @Override
  public String getHelp() {
    return "Fügt einen Mitarbeiter zu deinem Mallshop hinzu";
  }

  @Override
  public String getSyntax() {
    return this.getCommand() + " [shop] [spieler]";
  }

  @Override
  public String getPermission() {
    return "Mall.AddMember";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    if (args.length >= 2) {
      String mallRegionPattern = Mall.getInstance().getConfig().getString("regionPattern");

      String region = args[0];
      String player = args[1];

      if (player.equalsIgnoreCase(commandSender.getName())) {
        this.sendMessage(commandSender, "Du kannst dich nicht selbst als Mitarbeiter hinzufügen");
        return true;
      }

      OfflinePlayer member;

      if (player.startsWith("u:")) {
        player = player.substring(2);
        try {
          UUID playerID = UUID.fromString(player);
          member = Bukkit.getOfflinePlayer(playerID);
        } catch (IllegalArgumentException e) {
          this.sendMessage(commandSender, "\"" + player + "\" ist keine gültige UUID.");
          return true;
        }

        if (member == null) {
          this.sendMessage(commandSender, "Dieser Spieler war noch nie auf dem Server.");
          return true;
        }
      } else {
        member = Bukkit.getPlayer(player);

        if (member == null) {
          this.sendMessage(commandSender, String.format("Der Spieler %s ist zurzeit nicht online!", player));
          this.sendMessage(commandSender, "Du kannst ihn aber trotzdem über seine UUID hinzufügen:");
          this.sendMessage(commandSender, "\"/mall addmember <shop> u:<uuid>\" - \"/mall addmember m231 u:4a02d289-e2ae-4cb6-b598-2899a46c95ad\"");
          this.sendMessage(commandSender, "Die UUID kannst du z.B. mit https://namemc.com herausfinden.");
          return true;
        }
      }

      if (region.matches(mallRegionPattern)) {
        try {
          ProtectedRegion protectedRegion = Mall.getWorldGuardApi().getRegion(
                  Mall.getWorldGuardApi().getRegionManager(Bukkit.getWorld(Mall.getInstance().getConfig().getString("world"))),
                  region,
                  false
          );

          if (protectedRegion.getOwners().contains(commandSender.getName()) || commandSender.hasPermission("Mine.Admin")) {
            if (protectedRegion.getMembers().contains(member.getUniqueId())) {
              this.sendMessage(
                      commandSender,
                      String.format("Der Spieler %s ist bereits Mitarbeiter des MallShops %s.", member.getName() != null ? member.getName() : member.getUniqueId().toString(), region)
              );
            } else {
              protectedRegion.getMembers().addPlayer(member.getUniqueId());
              this.sendMessage(
                      commandSender,
                      String.format("Der Spieler %s wurde als Mitarbeiter zum MallShop %s hinzugefügt.", member.getName() != null ? member.getName() : member.getUniqueId().toString(), region)
              );
            }
          } else {
            this.sendMessage(commandSender, String.format("Du hast keine Berechtigung für den MallShop %s.", region));
          }
        } catch (CommandException e) {
          this.sendMessage(commandSender, String.format("\"%s\" ist keine gültige Shopnummer.", region));
        }
      } else {
        this.sendMessage(commandSender, String.format("\"%s\" ist keine gültige Shopnummer.", region));
      }

      return true;
    }

    return false;
  }
}
