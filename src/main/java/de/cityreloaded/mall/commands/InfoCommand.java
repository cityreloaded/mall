package de.cityreloaded.mall.commands;

import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import de.cityreloaded.mall.Mall;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by Lars on 06.11.2016.
 */
public class InfoCommand extends AbstractCommand {

  @Override
  public String getCommand() {
    return "info";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"mall"};
  }

  @Override
  public String getHelp() {
    return "Zeigt Infos zu einem Mall Shop";
  }

  @Override
  public boolean isPlayerOnly() {
    return true;
  }

  @Override
  public String getPermission() {
    return "Mall.Info";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    Player player = Bukkit.getPlayer(commandSender.getName());

    if (player.getWorld().getName().equals(Mall.getInstance().getConfig().getString("world"))) {
      String mallRegionPattern = Mall.getInstance().getConfig().getString("regionPattern");

      if (args.length > 0) {
        String regionName = args[0];

        if (regionName.matches(mallRegionPattern)) {
          try {
            ProtectedRegion mallShop = Mall.getWorldGuardApi().getRegion(
                    Mall.getWorldGuardApi().getRegionManager(player.getWorld()),
                    regionName, false
            );
            sendMessage(commandSender, "Shop " + mallShop.getId() + ":");
            ArrayList<String> players = new ArrayList<>();
            mallShop.getOwners().getUniqueIds().forEach(uuid -> {
              OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);
              if (offlinePlayer != null) {
                players.add(offlinePlayer.getName());
              }
            });
            String owners = String.join(", ", players);
            if (owners.length() > 0) {
              sendMessage(commandSender, "Besitzer: " + owners);
            } else {
              sendMessage(commandSender, "Dieser Shop hat keinen Besitzer");
            }
            players.clear();

            mallShop.getMembers().getUniqueIds().forEach(uuid -> {
              OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);
              if (offlinePlayer != null && offlinePlayer.getName() != null) {
                players.add(offlinePlayer.getName());
              }
            });
            String members = String.join(", ", players);
            if (members.length() > 0) {
              sendMessage(commandSender, "Mitarbeiter: " + members);
            } else {
              sendMessage(commandSender, "Dieser Shop hat keine Mitarbeiter");
            }
          } catch (CommandException e) {
            sendMessage(commandSender, "Das ist keine gültige Shopnummer!");
          }
        } else {
          sendMessage(commandSender, "Das ist keine gültige Shopnummer!");
        }
      } else {
        try {
          ProtectedRegion[] regions = Mall.getWorldGuardApi().getRegionStandingIn(player, false);

          for (ProtectedRegion region : regions) {
            if (region.getId().matches(mallRegionPattern)) {
              sendMessage(commandSender, String.format("Deine Shopnummer ist %s%s", ChatColor.GOLD, region.getId()));
              break;
            } else {
              sendMessage(commandSender, "Du befindest dich nicht in einem Mall-Shop");
            }
          }
        } catch (CommandException e) {
          sendMessage(commandSender, "Du befindest dich nicht in einem Mall-Shop");
        }
      }
    } else {
      this.sendMessage(commandSender, ChatColor.RED + "Du befindest dich nicht in der Mall.");
      this.sendMessage(commandSender,
              String.format("%sGib %s/warp mall %sein, um in die Mall zu gelangen.", ChatColor.RED, ChatColor.GOLD, ChatColor.RED)
      );
    }

    return true;
  }
}
