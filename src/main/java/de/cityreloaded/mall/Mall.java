package de.cityreloaded.mall;

import de.cityreloaded.mall.commands.AddMemberCommand;
import de.cityreloaded.mall.commands.InfoCommand;
import de.cityreloaded.mall.commands.RemoveMemberCommand;
import de.paxii.bukkit.commandapi.CommandApi;
import de.paxii.bukkit.commandapi.chat.Chat;
import de.paxii.bukkit.worlguardapi.WorldGuardApi;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;

/**
 * Created by Lars on 06.11.2016.
 */
public class Mall extends JavaPlugin {

  @Getter
  private static Mall instance;

  @Getter
  private CommandApi commandApi;

  @Getter
  private WorldGuardApi worldGuardApi;

  public static Chat getChat() {
    return Mall.instance.commandApi.getChat();
  }

  public static WorldGuardApi getWorldGuardApi() {
    return Mall.instance.worldGuardApi;
  }

  @Override
  public void onEnable() {
    Mall.instance = this;
    this.setupConfig();

    this.commandApi = new CommandApi();
    this.worldGuardApi = new WorldGuardApi();
    this.commandApi.setChatPrefix(ChatColor.GOLD + "[Mall] " + ChatColor.RESET);

    this.commandApi.addCommand(new InfoCommand());
    this.commandApi.addCommand(new AddMemberCommand());
    this.commandApi.addCommand(new RemoveMemberCommand());

    this.getCommand("mall").setExecutor(this.commandApi.getExecutor());
  }

  private void setupConfig() {
    this.getConfig().options().copyDefaults(true);
    this.saveConfig();
  }
}
